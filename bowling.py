import random

class Game1(object):


    def __init__(self):
        # print('Game1 created')
        self.frames_to_play = 9
        self.players = []
        self.current_frame = 0
        self.current_player = 0


    def create_player(self, name):
        print('\n Adding player of name: ', name)
        self.players.append(Player1(name))
        print('Players ')
        for player in self.players:
            print(player.name, sep=' ', end='', flush=True)
        print(' in Game')



    def next_players_turn(self):
        self.players[self.current_player].turn = False
        self.current_player += 1
        self.players[self.current_player].turn = True



    def check_next_frame(self):
        if self.current_player == len(self.players)-1:
            return True


    def next_frame(self):
        self.current_player = 0
        self.frames_to_play -= 1
        self.current_frame += 1

        print("\n \n###########")
        print('Frame', self.current_frame + 1)
        print("###########\n\n")



    def players_turn(self):
        player_taking_turn = self.players[self.current_player]
        player_taking_turn.turn = True
        print('\n\n', player_taking_turn.name,' \'s turn')
        player_taking_turn.take_turn()
        print(player_taking_turn.name, '\'s points in each frame so far are: ')
        for frame in player_taking_turn.points:
            print(frame.get_points(), sep=' ', end='', flush=True)
        print('\n calculate_points func')
        player_taking_turn.calculate_points()

    def play(self):
        print("\n \n###########")
        print('Frame 1')
        print("###########\n\n")
        while self.frames_to_play > -1:
            self.players_turn()
            if self.check_next_frame():
                self.next_frame()
            else:
                self.next_players_turn()



#     def calculate_points(self):
#         for i in range(len(self.points)):
#             print('frame number: ', i, ' frame score: ', self.points[i])
#                 # print('roll scores in frame: ', self.points[i][j])
#             if not self.strike(self.points[i]):
#                 print('not a strike')
#                 frame_score = sum(self.points[i])
#                 self.points_calculate.append(frame_score)
#                 print('frame rolls: ', self.points)
#                 print('points calculate: ', self.points_calculate)
#             if self.strike(self.points[i]):
#                 print('strike')
#                 if self.strike(self.points[i+1]):
#                     print('strike')
#                     self.points_calculate.append(self.points[i][0]+self.points[i+1][0]+self.points[i+2][0])
#                     print('frame rolls: ', self.points)
#                     print('points calculate: ', self.points_calculate)
#                 elif not self.strike(self.points[i+1]):
#                     print('next frame points: ', self.points[i+1])
#                     frame_score = self.points[i][0]+sum(self.points[i+1])
#                     self.points_calculate.append(frame_score)
#                     print('frame rolls: ', self.points)
#                 print('points calculate: ', self.points_calculate)

class Player1(object):


    def __init__(self, name):
        # print('Player 1 created')
        self.name = name
        self.points = []
        self.points_calculate = []
        self.turn = False

    def add_frame_to_score(self, frame_score):
        self.points.append(frame_score)

    def take_turn(self):
        if self.turn  == True:
            turn = Frame1()
            while not turn.players_frame_end():
                roll_score = turn.roll()
                # print('Roll score: ', roll_score)
                # print('Frame score: ', turn.get_points())

            self.add_frame_to_score(turn)

    def get_points(self):
        return self.points

    def calculate_points(self):
        for frame in self.points:
            print(frame.points, sep=' ', end='', flush=True)

            if frame.strike():
                print('strike')

            if frame.spare():
                print('spare')


class Frame1(object):
    def __init__(self):
        # print('A frame1 is beginning ')
        self.points = []
        self.rolls = 2
        self.pins = 10

    def players_frame_end(self):
        if self.rolls == 0 or self.pins == 0:
            # print('frame over')
            return True
        return False


    def roll(self):
        # print('a roll is being taken')
        roll_score = random.randint(0, self.pins)
        self.rolls -= 1
        self.pins -= roll_score
        # self.points += roll_score
        print('You\'ve knocked down %s pins' % (roll_score))
         # your score so far is %s' % (roll_score, self.points))
        self.points.append(roll_score)
        # return roll_score

    def get_rolls(self):
        return self.rolls


    def get_pins(self):
        return self.pins

    def get_points(self):
        return self.points

    def strike(self):
        if len(self.points) == 1 and self.points[0] == 10:
            return True

    def spare(self):
        if len(self.points) == 2 and sum(self.points) ==10:
            return True

if __name__ == '__main__':
    g= Game1()
    g.create_player('Liam')
    g.create_player('Andrew')
    g.play()
    # g.create_player('tester')
    # g.players[0].points.append([1,2])
    # g.players[0].points.append([3,4])
    # g.players[0].points.append([5,6])
    # g.players[0].points.append([7,8])
    # g.players[0].points.append([9,1])
    # print('players points: ', g.players[0].get_points())
    # g.players[0].points.append([10])
    # g.players[0].points.append([10])
    # g.players[0].points.append([10])
    # g.players[0].points.append([10])
    # g.players[0].points.append([10])
    # g.players[0].calculate_points()
    # g.players[1].calculate_points()
